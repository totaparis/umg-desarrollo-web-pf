# UMG Desarrollo Web PF

Consideraciones Generales:
Debe existir un sitio web ofreciendo explicando cada giro de negocio, ofreciendo los
servicios y productos a modo de sala de ventas fácilmente identificable; este sitio web también debe
poder ser el nexo entre los clientes.

Link de las aplicaciones:

->Landing
https://umg-landing-devweb.s3.amazonaws.com/index.html

->E-commerce
https://umg-ecommerce-devweb.s3.amazonaws.com/index.html#/

->Admin
https://umg-admin-devweb.s3.amazonaws.com/index.html#/auth/login

*Documentación*
https://gitlab.com/totaparis/umg-desarrollo-web-pf/wikis/pages