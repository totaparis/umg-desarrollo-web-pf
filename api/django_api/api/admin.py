from django.contrib import admin
# from .models import Hero
# from .models import billano
# Register your models here.
from .models import *


#admin.site.register(Hero)
#admin.site.register(billano)
admin.site.register(Empresa)
admin.site.register(GiroNegocio)
admin.site.register(Cliente)
admin.site.register(ProductoServicio)
admin.site.register(Pedido)
admin.site.register(PedidoProductoServicio)