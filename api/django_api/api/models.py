from django.db import models

# Create your models here.

# class Hero(models.Model):
#     name = models.CharField(max_length=60)
#     alias = models.CharField(max_length=60)
#     def __str__(self):
#         return self.name


# class billano(models.Model):
#     name_billano = models.CharField(max_length=100)
#     def __str__(self):
#         return self.name_billano



class Empresa(models.Model):
    nombre = models.CharField(max_length=60)
    def __str__(self):
        return self.nombre


class GiroNegocio(models.Model):
    idEmpresa = models.ForeignKey(Empresa,on_delete=models.CASCADE)
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=1000)
    ubicacion = models.CharField(max_length=500)

class ProductoServicio(models.Model):
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=1000)
    precio = models.IntegerField(default=0)
    idGiroNegcio = models.ForeignKey(GiroNegocio,on_delete=models.CASCADE)


class Cliente(models.Model):
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=100)
    dpi = models.CharField(max_length=20)
    direccion = models.CharField(max_length=500)
    telefono = models.IntegerField(default=0)

class Pedido(models.Model):
    fecha = models.DateField(auto_now_add=True)
    idCliente = models.ForeignKey(Cliente,on_delete=models.CASCADE)
    Estado = models.IntegerField(default=0)
    Total = models.IntegerField(default=0)

class PedidoProductoServicio(models.Model):
    idProdcutos = models.ForeignKey(ProductoServicio,on_delete=models.CASCADE)
    idPedido = models.ForeignKey(Pedido,on_delete=models.CASCADE)

class KmRecorrido(models.Model):
    idProdcutos = models.ForeignKey(ProductoServicio,on_delete=models.CASCADE)
    idPedido = models.ForeignKey(PedidoProductoServicio,on_delete=models.CASCADE)
    kilometrosRecorridos = models.IntegerField(default=0)




