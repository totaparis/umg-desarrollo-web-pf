from rest_framework import serializers
#from .models import Hero
#from .models import billano
from .models import *


"""class HeroSerializer(serializers.ModelSerializer):
    class Meta:
        model = Hero
        fields = '__all__'


class BillanoSerializer(serializers.ModelSerializer):
    class Meta:
        model = billano
        fields = '__all__'"""

class EmpresaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Empresa
        fields = '__all__'

class GiroNegocioSerializer(serializers.ModelSerializer):
    class Meta:
        model = GiroNegocio
        fields = '__all__'

class ClienteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cliente
        fields = '__all__'


class ProductoServicioSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductoServicio
        fields = '__all__'

class PedidoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pedido
        fields = '__all__'

class PedidoProductoServicioSerializer(serializers.ModelSerializer):
    class Meta:
        model = PedidoProductoServicio
        fields = '__all__'
