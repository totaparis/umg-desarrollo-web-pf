from django.urls import include, path
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'empresa', views.EmpresaViewSet)
router.register(r'giro', views.GiroNegocioViewSet)
router.register(r'cliente', views.ClienteViewSet)
router.register(r'producto', views.ProductoServicioViewSet)
router.register(r'pedido', views.PedidoClienteViewSet)
router.register(r'pedprodser', views.PedidoProductoServicioViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]